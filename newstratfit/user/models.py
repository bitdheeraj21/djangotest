from django.db import models

# Create your models here.

class User(models.Model):
    name = models.CharField(max_length=50)
    GENDER_CHOICES = (
        ('M', 'male'),
        ('F', 'female'),
    )
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, null=True, blank= True)
    dob = models.DateField(max_length=8, blank=True, null= True)

